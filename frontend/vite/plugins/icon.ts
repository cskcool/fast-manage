import Icons from "unplugin-icons/vite";

export function setupUnpluginIcons(plugins: any) {
  plugins.push(
    Icons({
      autoInstall: true
    })
  );
}
