import { RouteRecordRaw } from "vue-router";

export default {
  name: "permission",
  path: "/permission",
  component: () => import("@/layouts/common-page.vue"),
  meta: { auth: true, menu: { title: "router.permission", icon: "Lock" } },
  children: [
    {
      name: "userList",
      path: "userList",
      component: () => import("@/views/permission/UserList.vue"),
      meta: { menu: { title: "router.userList" } }
    },
    {
      name: "roleManage",
      path: "roleManage",
      component: () => import("@/views/permission/roleManage.vue"),
      meta: { menu: { title: "router.roleManage" }, permissions: ["editor", "admin"] }
    },
    {
      name: "objectManage",
      path: "objectManage",
      component: () => import("@/views/permission/objectManage.vue"),
      meta: { menu: { title: "router.objectManage" }, permissions: ["admin"] }
    },
    {
      name: "actionManage",
      path: "actionManage",
      component: () => import("@/views/permission/actionManage.vue"),
      meta: { menu: { title: "router.actionManage" }, permissions: ["editor", "admin"] }
    }
  ]
} as RouteRecordRaw;
