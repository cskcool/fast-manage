import { BasicResult } from "#/resultType";
import { RolePermission } from "@/model/role";
import { http } from "@/utils/http";
import { debug } from "console";

enum API {
  FETCH_ROLE_PERMISSION_INFO = "/role/get_coca",
  CHANGE_ROLE_PERMISSION = "/role/change_role"
}

export interface ILoginForm {
  account: string;
  password: string;
}
/**
 *
 * @description 获取角色对应的权限信息
 */
export const fetchRolePermissionInfo = (data: Partial<RolePermission>) => {
  return new Promise<BasicResult<RolePermission>>(async (resolve, reject) => {
    try {
      const res = await http.get<{}, BasicResult<RolePermission>>(API.FETCH_ROLE_PERMISSION_INFO, data);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

/**
 *
 * @description 修改角色对应的权限信息
 */
 export const changeRole = (data: Partial<RolePermission>) => {
  return new Promise<BasicResult<{ access_token: string }>>(async (resolve, reject) => {
    try {
      const res = await http.post<{}, BasicResult<{ access_token: string }>>(API.CHANGE_ROLE_PERMISSION, {
        data
      });
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

