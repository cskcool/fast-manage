import { CTableColumn } from "#/table";
import { objectModel } from "@/model/object";
import { UrlListType } from "@/utils/list/listFactory";
import { useI18n } from "vue-i18n";

export const objectUrl: Partial<UrlListType> = {
  list: "/co/get_cos",
  delete: "/co/delete_co",
  getDetail: "/co/get_co",
  edit: "/co/update_co",
  add: "/co/create_co"
};

export const setupArticleAttributes = () => {
  const { t } = useI18n();

  const articleFilterOptions = computed(() => {
    return [
      {
        label: t("page.common.permission.user.search.title"),
        name: "keyword",
        tagName: "el-input",
        props: {
          placeholder: t("page.common.permission.user.search.title_placeholder"),
          maxLength: "24"
        }
      },
      // https://github.com/cloudhao1999/cloud-app-admin/issues/17
      // {
      //   label: t("page.common.permission.user.search.type"),
      //   name: "test",
      //   type: "select",
      //   props: {
      //     style: {
      //       width: "100%"
      //     }
      //   },
      //   children: [
      //     {
      //       tagName: "el-option",
      //       props: {
      //         label: "test1",
      //         value: "test1"
      //       }
      //     },
      //     {
      //       tagName: "el-option",
      //       props: {
      //         label: "test2",
      //         value: "test2"
      //       }
      //     }
      //   ]
      // }
    ];
  });

  const articleColumns = computed<CTableColumn<objectModel>[]>(() => {
    return [
      // {
      //   type: "selection",
      //   show: true,
      //   width: "55"
      // },
      {
        prop: "id",
        show: true,
        label: t("page.common.permission.user.column.id"),
        width: "80"
      },
      {
        prop: "name",
        show: true,
        label: t("page.common.permission.user.column.title"),
        width: "180"
      },
      {
        prop: "description",
        show: true,
        label: t("page.common.permission.user.column.content"),
        showOverflowTooltip: true
      },
      {
        prop: "actions",
        show: true,
        label: t("page.common.permission.user.column.action"),
        fixed: "right",
        scoped: "actions",
        width: "150"
      }
    ];
  });

  return {
    articleFilterOptions,
    articleColumns
  };
};
