import { CTableColumn } from "#/table";
import { RoleModel } from "@/model/role";
import { UrlListType } from "@/utils/list/listFactory";
import { useI18n } from "vue-i18n";

export const roleUrl: Partial<UrlListType> = {
  list: "/role/get_roles",
  delete: "/role/delete_role",
  getDetail: "/role/get_coca",
  edit: "/role/update_role",
  add: "/role/create_role"
};

export const setupRoleAttributes = () => {
  const { t } = useI18n();

  const roleFilterOptions = computed(() => {
    return [
      {
        label: t("page.common.permission.role.form.roleName"),
        name: "name",
        tagName: "el-input",
        props: {
          placeholder: t("page.common.permission.role.form.roleName_placeholder"),
          maxLength: "24"
        }
      },
      // https://github.com/cloudhao1999/cloud-app-admin/issues/17
      // {
      //   label: t("page.common.permission.user.search.type"),
      //   name: "test",
      //   type: "select",
      //   props: {
      //     style: {
      //       width: "100%"
      //     }
      //   },
      //   children: [
      //     {
      //       tagName: "el-option",
      //       props: {
      //         label: "test1",
      //         value: "test1"
      //       }
      //     },
      //     {
      //       tagName: "el-option",
      //       props: {
      //         label: "test2",
      //         value: "test2"
      //       }
      //     }
      //   ]
      // }
    ];
  });

  const roleColumns = computed<CTableColumn<RoleModel>[]>(() => {
    return [
      // {
      //   type: "selection",
      //   show: true,
      //   width: "55"
      // },
      {
        prop: "id",
        show: true,
        label: t("page.common.permission.user.column.id"),
        width: "80"
      },
      {
        prop: "name",
        show: true,
        label: t("page.common.permission.role.form.roleName"),
        width: "180"
      },
      {
        prop: "description",
        show: true,
        label: t("page.common.permission.user.column.content"),
        showOverflowTooltip: true
      },
      {
        prop: "actions",
        show: true,
        label: t("page.common.permission.user.column.action"),
        fixed: "right",
        scoped: "actions",
        width: "250"
      }
    ];
  });

  return {
    roleFilterOptions,
    roleColumns
  };
};
