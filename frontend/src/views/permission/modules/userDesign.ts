import { CTableColumn } from "#/table";
import { userListModel } from "@/model/userList";
import { UrlListType } from "@/utils/list/listFactory";
import { useI18n } from "vue-i18n";

export const userListUrl: Partial<UrlListType> = {
  list: "/user/get_users",
  delete: "/user/delete_user",
  batchDelete: "/user/delete_user",
  edit: "/user/update_user",
  add: "/user/create_user"
};

export const setupUserListAttributes = () => {
  const { t } = useI18n();

  const userListFilterOptions = computed(() => {
    return [
      {
        label: t("page.common.permission.user.search.title"),
        name: "keyword",
        tagName: "el-input",
        props: {
          placeholder: t("page.common.permission.user.search.title_placeholder"),
          maxLength: "24"
        }
      }
      // https://github.com/cloudhao1999/cloud-app-admin/issues/17
      // {
      //   label: t("page.common.permission.user.search.type"),
      //   name: "test",
      //   type: "select",
      //   props: {
      //     style: {
      //       width: "100%"
      //     }
      //   },
      //   children: [
      //     {
      //       tagName: "el-option",
      //       props: {
      //         label: "test1",
      //         value: "test1"
      //       }
      //     },
      //     {
      //       tagName: "el-option",
      //       props: {
      //         label: "test2",
      //         value: "test2"
      //       }
      //     }
      //   ]
      // }
    ];
  });

  const userListColumns = computed<CTableColumn<userListModel>[]>(() => {
    return [
      {
        type: "selection",
        show: true,
        width: "55"
      },
      {
        prop: "id",
        show: true,
        label: t("page.common.permission.user.column.id"),
        width: "80"
      },
      {
        prop: "username",
        show: true,
        label: t("page.common.permission.user.column.title"),
        width: "180"
      },
      {
        prop: "remark",
        show: true,
        label: t("page.common.permission.user.column.content"),
        showOverflowTooltip: true
      },
      {
        prop: "sex",
        show: true,
        label: t("page.common.permission.user.form.sex"),
        scoped: "sex",
        showOverflowTooltip: true
      },
      {
        prop: "actions",
        show: true,
        label: t("page.common.permission.user.column.action"),
        fixed: "right",
        scoped: "actions",
        width: "150"
      }
    ];
  });

  return {
    userListFilterOptions,
    userListColumns
  };
};
