export interface RoleModel {
    name: string;
    role_key: string;
    description: string;
    user_id: string;
    roleId: string;
    id: string;
  }
  
export interface RolePermission {
    role_id: number;
    checkeds: any;
    options: any;
  }