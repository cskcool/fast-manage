export interface userListModel {
  id: number;
  username: string;
  remark: string;
  type: string;
  sex: string;
  password: string;
  email: string;
  avatar: string;
  user_id: string;
}
