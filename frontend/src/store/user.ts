import { fetchAdminInfo, fetchEditorInfo, ILoginForm, userLogin } from "@/api/user";
import permissionService from "@/hooks/usePermission";
import { UserInfoModel } from "@/model/user";
import { setToken } from "@/utils/auth";
import { defineStore } from "pinia";
import avatarImage from "@/assets/img/avatar.jpg"

export const userStore = defineStore("user", () => {
  const info = ref<Partial<UserInfoModel>>({});

  const username = computed(() => {
    return info.value!.username;
  });
  const avatar = computed(() => {
    return info.value!.avatar || avatarImage;
  });
  const isEmpty = computed(() => {
    return Object.keys(info.value!).length === 0;
  });
  const permission = computed(() => {
    return info.value?.permission;
  });
  const roleList = computed(() => {
    return info.value?.roleList;
  });

  async function getUserInfo() {
    const permissions = permissionService.defaultPermission;
    let data;
    if (permissions.value === "admin") {
      ({ data } = await fetchAdminInfo());
    } else {
      ({ data } = await fetchEditorInfo());
    }
    if (data) {
      info.value = data;
    }
  }
  async function login(loginParam: ILoginForm, success: () => void, error: (err: any) => void) {
    try {
      const {
        data: { access_token }
      } = await userLogin(loginParam);
      if (access_token) {
        setToken(access_token);
        await getUserInfo();
        success();
      } else {
        throw "获取token失败";
      }
    } catch (err) {
      error(err);
    }
  }

  return {
    info,
    username,
    avatar,
    isEmpty,
    permission,
    roleList,
    login,
    getUserInfo
  };
});
