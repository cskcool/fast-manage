
import os
import sys

# 将当前目录添加到系统变量中
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(BASE_DIR)

from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware  # 解决跨域
from fastapi.exceptions import RequestValidationError
from fastapi.responses import PlainTextResponse
from starlette.exceptions import HTTPException as StarletteHTTPException
from api.api_v1.tools.json_resonse import sucessResonse, errorResponse
import uvicorn as uvicorn

from database import Base, engine, get_db
from api.api_v1.access_token import access_token_router
from api.api_v1.user import user_router
from api.api_v1.role import role_router
from api.api_v1.casbin import casbin_router
import crud

from fastapi.responses import HTMLResponse  # 响应html
from fastapi.staticfiles import StaticFiles  # 设置静态目录

__version__ = "0.1.1"
description = '''Mini Admin,一个简洁轻快的后台管理框架.支持拥有多用户组的RBAC管理后台 🚀'''

app = FastAPI(
    title="MiniAdmin",
    description=description,
    version=__version__,
    terms_of_service="#",
    license_info={
        "name": "MIT",
        "url":  "https://opensource.org/licenses/MIT",
    },
)

# 配置允许域名
origins = [
    "http://localhost",
    "http://localhost:5173",
    "http://127.0.0.1:5173",

]
# 配置允许域名列表、允许方法、请求头、cookie等
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(access_token_router)
app.include_router(user_router)
app.include_router(role_router)
app.include_router(casbin_router)

# 静态资源
app.mount("/dist", StaticFiles(directory=os.path.join(BASE_DIR, 'dist')), name="dist")
app.mount("/assets", StaticFiles(directory=os.path.join(BASE_DIR, 'dist/assets')), name="assets")

# 删除表，当更新表的结构时可以使用，但是会删除所有数据。慎用！！！！
# models.Base.metadata.drop_all(bind=engine)
# 在数据库中生成表结构
Base.metadata.create_all(bind=engine)
# 生成初始化数据，添加了一个超级管理员并赋予所有管理权限，以及一些虚拟的用户。
crud.create_data(next(get_db()))


@app.get("/")
def main():
    html_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'dist', 'index.html')
    html_content = ''
    with open(html_path, encoding="utf-8") as f:
        html_content = f.read()
    return HTMLResponse(content=html_content, status_code=200)

@app.exception_handler(StarletteHTTPException)
async def http_exception_handler(request, exc):
    return PlainTextResponse(str(exc.detail), status_code=exc.status_code)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    return errorResponse(data = str(exc))
    # return PlainTextResponse({'data': errorResponse(data = exc)}, status_code=400)


if __name__ == '__main__':
    host_ip = '127.0.0.1'
    if len(sys.argv) > 1:
        host_ip = sys.argv[1]
    uvicorn.run(app='main:app', host=host_ip, port=8000, reload=True, )
