# -*- coding: UTF-8 -*-
from typing import Union

from pydantic import BaseModel

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    username: Union[str, None] = None

# 角色管理
class UserBase(BaseModel):
    email: str

class UserCreate(UserBase):
    username: str
    password: str
    sex: str
    remark: str

class User(UserBase):
    id: int
    username: str
    sex: str
    email: str
    is_active: bool
    avatar: Union[str, None] = None
    remark: Union[str, None] = None

    class Config:
        orm_mode = True

class UserUpdate(BaseModel):
    id: int
    username: str
    password: Union[str, None] = ''
    sex: str
    email: str
    avatar: Union[str, None] = None
    remark: Union[str, None] = None

class Users(BaseModel):
    users: list[User]
    count: int

class roleBase(BaseModel):
    name: str

class Role(roleBase):
    name: str
    role_key: str
    description: str
    user_id: str
    id: str
    
    class Config:
        orm_mode = True

class Roles(BaseModel):
    list: list[Role]
    count: int

class CreateRole(BaseModel):
    user_id: int
    name: str
    role_key: str
    description: str

class UpdateRole(BaseModel):
    id: int
    name: str
    role_key: str
    description: str

# 资源管理
class CasbinObjectBase(BaseModel):
    name: str

class CasbinObject(CasbinObjectBase):
    name: str
    object_key: str
    description: str
    id: str
    
    class Config:
        orm_mode = True

class CasbinObjects(BaseModel):
    list: list[CasbinObject]
    count: int

class EditRole(BaseModel):
    old_role_id: int
    name: str
    role_key: str
    description: str

class createCasbinObject(BaseModel):
    name: str
    object_key: str
    description: str
    user_id: int

class EditCasbinObject(BaseModel):
    id: int
    name: str
    object_key: str
    description: str

# 动作管理
class CasbinActionBase(BaseModel):
    name: str

class CasbinAction(CasbinActionBase):
    name: str
    action_key: str
    description: str
    id: str
    
    class Config:
        orm_mode = True

class CasbinActions(BaseModel):
    list: list[CasbinAction]
    count: int

class createCasbinAction(BaseModel):
    name: str
    action_key: str
    description: str
    user_id: int

class EditCasbinAction(BaseModel):
    id: int
    name: str
    action_key: str
    description: str

class ChangeRole(BaseModel):
    role_id: int
    checkeds: list

class ChangeUserRole(BaseModel):
    user_id: int
    names: list[str]

class Casbin_rule(BaseModel):
    obj: str
    act: str

class login(BaseModel):
    username: str
    password: str
