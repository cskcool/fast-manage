### 计划做一个基于 fastapi + vue + elementPlus 的后台管理系统

***后台基于开源项目 miniadmin***

***前端基于开源项目 cloud-app-admin***
**目前完成了登陆功能的联调及角色权限列表页的显示**
*如何启动：*

 1. `pip install -r requirements.txt 安装 python 依赖` 
 2. `进入 backend 文件夹，终端输入 python main.py 启动后台项目`
 3. `浏览器输入 127.0.0.1:8000 即可访问`
![首页](img/01.png)

